<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="device" var="device"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	
	var fromDashboard = "${fromDashBoard}";
	
	var $stopped = "${stopped}";
	var $incycle = "${incycle}";
	var $stop_time = "${stop_time}";
	var $reference_date = "${reference_date}";
	var $compl = "${compl}";
	var $not_operating = "${not_operating}";
	
	
	
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/moment.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-rotate.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}

/* input#sDate.date.hasDatepicker{
font-size : 20px !important;
} */


</style> 
<script type="text/javascript">
	
	var jigData;
	
	function minusSdate(){
	 	var date2 = $("#sDate").datepicker('getDate', '+1d'); 
	  	date2.setDate(date2.getDate()-1); 
	  	$("#sDate").datepicker('setDate', date2);
	  	//getNightlyMachineStatus()
	}
	
	function plusSdate(){
		var date2 = $("#sDate").datepicker('getDate');
	  	date2.setDate(date2.getDate()+1); 
	  	$("#sDate").datepicker('setDate', date2);
	  	//getNightlyMachineStatus()
	}
	
	function minusEdate(){
	 	var date2 = $("#eDate").datepicker('getDate');
	 	console.log("eDate : " + $("#eDate").datepicker('getDate'));
	  	date2.setDate(date2.getDate()-1); 
	  	$("#eDate").datepicker('setDate', date2);
	  	//getNightlyMachineStatus()
	}
	
	function plusEdate(){
		var date2 = $("#eDate").datepicker('getDate', '+1d'); 
	  	date2.setDate(date2.getDate()+1); 
	  	$("#eDate").datepicker('setDate', date2);
	  	//getNightlyMachineStatus()
	}

	function setToday(){
		$(".date").val(getToday().substr(0,10))	
	};
	
	var appId = 25;
	
	var addAppFlag = true;
	var categoryId = 1;
	
	var appServerUrl = "http://52.169.201.78:8080/App_Store/index.do?categoryId=1";
	window.addEventListener("message", receiveMessage, false);
	
	function receiveMessage(evet){
		var msg = event.data;
		
		$("#app_store_iframe").animate({
			"left" : - $("#app_store_iframe").width() * 1.5 
		}, function(){
			$("#app_store_iframe").css({
				"left" : originWidth,
				"top" : (originHeight/2) - ($("#app_store_iframe").height()/2) 
			})
			
			addAppFlag = true;
			$(".addApp").rotate({
				duration:500,
			    angle: 45,
			   	animateTo:0
			});
		});
		
		fileDown(msg.url, msg.appName, msg.appId)
	}
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	
	var progressLoop = false;
	function getProgress(){
		var url = "${ctxPath}/getProgress.do";
		
		$.ajax({
			url : url,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data<100){
					$(".progressBar").remove();
					//draw ProgressBar();
					var targetBar = $(".nav_span").next("img").width();
					var currentBar = targetBar * Number(data).toFixed(1)/100;
					
					
					var barHeight = getElSize(50);
					var _top = $(".nav_span:nth(" + appCnt + ")").parent("td").offset().top + $(".nav_span:nth(" + appCnt + ")").parent("td").height() - barHeight;
					var _left = $(".nav_span:nth(" + appCnt + ")").parent("td").offset().left;
					
					var bar = document.createElement("div");
					bar.setAttribute("class", "progressBar");
					bar.style.cssText = "width : " + currentBar + "px;" + 
										"height : " + barHeight + "px;" +
										"color : #8D8D8D;" +
										"position : absolute;" + 
										"top : " + _top + "px;" + 
										"left : " + _left + "px;" +
										"z-index : 99999;" + 
										"background-color : lightgreen;";
										
										
					$("body").prepend(bar);
					
					clearInterval(progressLoop);
					progressLoop = setTimeout(getProgress,500)
					
					$(".nav_span:nth(" + appCnt + ")").html(Number(data).toFixed(1) + "%").css("color" , "#8D8D8D") 
				}else{
					resetProgress();
				}
				
				$(".addApp").remove();
			}
		});
	};
	
	function resetProgress(){
		var url = "${ctxPath}/resetProgress.do";
		
		$.ajax({
			url : url,
			type : "post",
			success : function(data){
			}
		});
	};
	
	
	
	function removeApp(appId){
		var url = "${ctxPath}/removeApp.do";
		var param = "appId=" + appId + 
					"&shopId=" + shopId;
		
		//console.log(param)
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					$("#delDiv").animate({
						"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
						"top" : - $("#delDiv").height() * 2
					})	
					
					$(".minus").animate({
						"width" : 0
					}, function(){
						$(this).remove();
					});
					getAppList();
				}
			}
		});
	};
	
	var appArray = [];
	var appCnt = 0;
				
	
	
	
	function fileDown(appUrl, appName, appId){
		var url = "${ctxPath}/fileDown.do";
		var param = "fileLocation=" + appUrl + 
					"&fileName=" + appName + 
					"&appId=" + appId + 
					"&ty=" + categoryId + 
					"&shopId=" + shopId;

		getProgress();
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				if(data=="success"){
					getAppList()					
				}
			}, error : function(e1,e2,e3){
				console.log(e1,e2,e3)
			}
		});
	};
	
	function showMinusBtn(parentTd, appId){
		$(".minus").animate({
			"width" : 0
		}, function(){
			$(this).remove();
		});
		
		var img = document.createElement("img");
		img.setAttribute("src", "${ctxPath}/images/minus.png");
		img.setAttribute("class", "minus");
		img.style.cssText = "position : absolute;" +
							"cursor : pointer;" + 
							"z-index: 9999;" + 
							"left : " + (parentTd.offset().left + parentTd.width()) + "px;" +
							"top : " + (parentTd.offset().top) + "px;" +
							"width : " + getElSize(0) + "px;";
		
		$(img).click(function(){
			$("#delDiv").animate({
				"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
				"top" : (window.innerHeight/2) - ($("#delDiv").height()/2) + getElSize(100)
			}, function(){
				$("#delDiv").animate({
					"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
					"top" : (window.innerHeight/2) - ($("#delDiv").height()/2)
				}, 100)
			});
			
			$("#delDiv div:nth(0)").click(function(){
				removeApp(appId);
			})
		});
		
		$(img).animate({
			"width" : getElSize(80)
		});		
		
		$("body").prepend(img)					
	};
	
	function getAppList(){
		var url = "${ctxPath}/getAppList.do";
		var param = "categoryId=" + categoryId + 
					"&shopId=" +shopId;
		
		appArray = [];
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
		
				$(".nav_span").html("");
				$(".addApp").remove();
				
				//id, appId, name
				
				appCnt = json.length;
				
				$(".nav_span").next("img").unbind("contextmenu").attr("src", "${ctxPath}/images/unselected.png");
				$(".nav_span").unbind("contextmenu");
				
				$(json).each(function(idx, data){
					appArray.push(data.appId)
					$(".nav_span:nth(" + idx + ")").attr("appId", data.appId);
					$(".nav_span:nth(" + idx + ")").html(data.name);
					$(".nav_span:nth(" + idx + ")").click(function(){
						location.href = "/" + data.name + "/index.do";	
					});
					
					$(".nav_span:nth(" + idx + ")").next("img").click(function(){
						if(data.name=="DashBoard"){
							location.href = "/DIMF/chart/main.do?categoryId=1";	
						}else{
							location.href = "/" + data.name + "/index.do";	
						}
							
					});
					
					$(".nav_span:nth(" + idx + ")").next("img").attr("appId", data.appId)
					
					if(data.appId==appId){
						$(".nav_span:nth(" + idx + ")").next("img").attr("src", "${ctxPath}/images/selected_blue.png")
						$(".nav_span:nth(" + idx + ")").css("color", "white");
					}
					$(".nav_span:nth(" + idx + ")").next("img").contextmenu(function(e){
						e.preventDefault();
						var appId = $(this).prev("span").attr("appId");
						var parentTd = $(".nav_span:nth(" + idx + ")").parent("Td");
						showMinusBtn(parentTd, appId)						
					});
					
					$(".nav_span:nth(" + idx + ")").contextmenu(function(e){
						e.preventDefault();
						var appId = $(this).attr("appId");
						var parentTd = $(".nav_span:nth(" + idx + ")").parent("Td")
						showMinusBtn(parentTd, appId)						
					});
				});
				
				var totalCell = 10;
				
				var parentTd = $(".nav_span:nth(" + json.length + ")").parent("td");

				var img = document.createElement("img");
				img.setAttribute("src", "${ctxPath}/images/add.png");
				img.setAttribute("class", "addApp");
				img.style.cssText = "position : absolute;" +
									"cursor : pointer;" + 
									"z-index: 9999;" + 
									"left : " + (parentTd.offset().left + (parentTd.width()/2) - getElSize(40)) + "px;" +
									"top : " + (parentTd.offset().top + (parentTd.height()/2) - getElSize(40)) + "px;" +
									"width : " + getElSize(80) + "px;"; 
									
					
				$("body").prepend(img);
				
				//iframe 호출
				$(img).click(function(){
					document.querySelector("#app_store_iframe").contentWindow.postMessage(appArray, '*');
					
					if(addAppFlag){
						$(this).rotate({
					   		duration:500,
					      	angle: 0,
					      	animateTo:45
						});
						$("#app_store_iframe").animate({
							"left" : (originWidth/2) - ($("#app_store_iframe").width()/2)
						});	
					}else{
						$(this).rotate({
							duration:500,
						    angle: 45,
						   	animateTo:0
						});
						$("#app_store_iframe").animate({
							"left" : - $("#app_store_iframe").width() * 1.5 
						}, function(){
							$("#app_store_iframe").css({
								"left" : originWidth,
								"top" : (originHeight/2) - ($("#app_store_iframe").height()/2) 
							})
						});
					}
					
					addAppFlag = !addAppFlag;
				})
			}
		});
	};
	
	$(function(){
		//getAppList();
		
		setToday()
		createNav("monitor_nav", 1)
		time();
		
		setDivPos();
		getNightlyMachineStatus();
	});
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setDivPos(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			//"background-color" : "black",
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(35),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$(".content").css({
			"height" : getElSize(400),
			"background-color" : "#191919",
			"color" : "white",
			"text-align" : "center",
			"font-size" : getElSize(170),
		});
		
		//1043
		
		$("img").css({
			"display" : "inline"
		})
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("select, input").css({
			"font-size" : getElSize(30),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(50),
		});
		
		$(".tmpTable td, .thead td").css({
		//	"color" : "##BFBFBF",
			"font-size" : getElSize(20),
			"padding" :getElSize(15)
		});
		
		$(".tmpTable2").css({
			"width" : "100%",
			"color": "white",
			"text-align": "center",
			"border-collapse" : "collapse",
			"margin-bottom" : getElSize(20)
		});
				
		
		$("#app_store_iframe").css({
			"width" : getElSize(3100) + "px",
			"height" : getElSize(2000) + "px",
			"position" : "absolute",
			"z-index" : 9999,
			"display" : "block"
		});
		
		$("#app_store_iframe").css({
			"left" : originWidth * 1.5,
			"top" : (originHeight/2) - ($("#app_store_iframe").height()/2) 
		}).attr("src", appServerUrl)
		
		
		$("#delDiv").css({
			"color" : "white",
			"z-index" : 9999999,
			"background-color" : "black",
			"position" : "absolute",
			"width" : getElSize(700) + "px",
			"font-size" : getElSize(60) + "px",
			"text-align" : "center",
			"padding" : getElSize(30) + "px",
			"border" : getElSize(7) + "px solid rgb(34,34,34)",
			"border-radius" : getElSize(50) + "px"
		});
		
		
		$("#delDiv div").css({
			"background-color" : "rgb(34,34,34)",
			"padding" : getElSize(20) + "px",
			"margin" : getElSize(10) + "px",
			"cursor" : "pointer"
		}).hover(function(){
			$(this).css({
				"background-color" : "white",
				"color" : "rgb(34,34,34)",
			})
		}, function(){
			$(this).css({
				"background-color" : "rgb(34,34,34)",
				"color" : "white",
			})
		});
		
		$("#delDiv").css({
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			"top" : - $("#delDiv").height() * 2
		});
		
		$("#delDiv div:nth(1)").click(function(){
			$("#delDiv").animate({
				"top" : - $("#delDiv").height() * 2
			});
		})
		
		$('.buttonDate').css({
			"width" : getElSize(70) *  1.2,
			"height" : getElSize(70),
			"margin-left" : getElSize(15)
		});
		
		$("#downLoad").css({
			"background" : "rgb(250,235,255)",
			"padding" : getElSize(10),
			"color" : "black",
			"font-size" : getElSize(45),
			"cursor":"pointer",
			"margin-left" : getElSize(20)
		})
		
		/*
			* Date : 19.05.14 
			* Author : wilson
			* change "top", "left", "width"
		*/
		if(getParameterByName('lang')=='ko'){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='en'){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='cn'){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='de'){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}
		
		$(".date").css({
			"font-size" : getElSize(46)
		});
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(60)
		});
	};
	
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	function csvSend(){
		var id = this.id;
		var sDate, eDate;
		var csvOutput;
		
		sDate = $("#sDate").val();
		eDate = "";
		csvOutput = jigData;
		
		
		csvOutput = csvOutput.replace(/\n/gi,"");
		
		f.csv.value=encodeURIComponent(csvOutput);
		f.startDate.value = sDate;
		f.endDate.value = eDate;
		f.submit();
	};
	
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
		<input type="hidden" name="title" value="Night">
	</form>


	<div id="delDiv">
		<spring:message  code="chk_del"></spring:message>
		<div><spring:message  code="check"></spring:message></div>
		<div><spring:message  code="cancel"></spring:message></div>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>
	
	
	<div id="dashBoard_title">${dmm_title}</div>
	

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" style="display: none">
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right' style="display: none">
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/monitor_left.png" class='menu_left'  style="display: none">
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/blue_right.png" class='menu_right' style="display: none">
				</td>
			</tr>
			<Tr>
				<Td>
					<span  class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table class="tmpTable2">
						<tr>
							<td colspan="6">
								<spring:message code="date_"></spring:message>
								<button class="buttonDate" onclick="plusSdate()">▲</button>
								<button class="buttonDate" onclick="minusSdate()">︎▼︎</button>
																
								<input type="text" id="sDate" class="date">
								
								~
								
								<button class="buttonDate" onclick="plusEdate()">▲</button>
								<button class="buttonDate" onclick="minusEdate()">︎▼︎</button>
																
								<input type="text" id="eDate" class="date">
								
								<img alt="" src="${ctxPath }/images/search.png" id="search" onclick="getNightlyMachineStatus()">
									
								
								
								<!-- <input type="date" class="date" id="sDate" onchange="handler(event);"> -->
								<%-- <img alt="" src="${ctxPath }/images/search.png" id="search" onclick="getNightlyMachineStatus()"> --%>
								
								<%-- <button onclick=""><spring:message code="excel"></spring:message></button> --%>
								<button id="downLoad" onclick="csvSend()" ><spring:message code="excel"></spring:message> </button>
							</td>
						</tr>
					</table>
					<div id="wrapper">
						<center>
							<table style="width: 80%;  color: white; text-align: center; border-collapse: collapse;" class="tmpTable" border="1">
								<thead>
									<Tr style="font-weight: bolder;background-color: rgb(34,34,34)" class="thead">
										<td>No</td>
										<td><spring:message code="machine_name"></spring:message></td>
										<td><spring:message code="date_"></spring:message></td>
										<td><spring:message code="stop_time"></spring:message></td>
										<td><spring:message code="status"></spring:message></td>
										<td><spring:message code="reference_date"></spring:message></td>
									</Tr>
								</thead>
							</table>
						</center>
					</div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
		</table>
	 </div>
	
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	

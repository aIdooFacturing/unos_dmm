package com.unomic.dulink.chart.service;

import java.util.List;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.domain.DeviceVo;
import com.unomic.dulink.chart.domain.NightChartVo;


public interface ChartService {
	public List<NightChartVo> getNightlyDvcStatus(DeviceVo inputVo);
	
	//common func
	public String getAppList(ChartVo chartVo) throws Exception;
	public String removeApp(ChartVo chartVo) throws Exception;
	public String addNewApp(ChartVo chartVo) throws Exception;
	public String login(ChartVo chartVo) throws Exception;
	public String getStartTime(ChartVo chartVo) throws Exception;
	public String getComName(ChartVo chartVo) throws Exception;
	public ChartVo getBanner(ChartVo chartVo) throws Exception;
	
};

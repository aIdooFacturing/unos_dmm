
var className = "";
var classFlag = true;
function getNightlyMachineStatus(){
	var url = ctxPath + "/getNightlyMachineStatus.do";

	var param = "stDate=" + $("#sDate").val() + 
				"&edDate=" + $("#eDate").val() +
				"&shopId=" + shopId;

	$.ajax({
		url : url,
		data : param,
		dataType : "text",
		type : "post",
		success : function(data){
			var json = $.parseJSON(data);
			
			console.log(data);
			
			var noConn=[];
			
			var tr = "";
					
			jigData = "No, 설비명, 날짜, 정지시간, 상태, 기준 날짜LINE";
						
			var status = "";
			var backgroundColor = "";
			var fontColor = "";
			
			$(".content").remove();
			
			$(json).each(function(idx, data){
				
				//console.log(data)
				
				if(classFlag){
					className = "row2"
				}else{
					className = "row1"
				};
				classFlag = !classFlag;
				
				
				console.log(data.status)
				if(data.status=="WAIT" || data.status=="NO-CONNECTION"){
					json[idx].status = $compl;
					json[idx].fontColor = "black";
					json[idx].backgroundColor = "yellow";
					
					var year = moment($("#sDate").val()).subtract(1,'days').format("YYYY")
					
					if(moment(year+'-'+data.stopDate + ' ' + data.stopTime).isBefore(moment($("#sDate").val()).subtract(1,'days').format("YYYY-MM-DD") + ' 18:01:00')){
						json[idx].status='미가동'
						json[idx].fontColor = "white";
						json[idx].backgroundColor = "black";
						json[idx].stopDate="-";
						json[idx].stopTime="-";
						noConn.push(idx)
					}
					
				}else if(data.status=='ALARM'){
					json[idx].status = $stopped;
					json[idx].fontColor = "white";
					json[idx].backgroundColor = "red";
				}else if(data.status=='IN-CYCLE'){
					json[idx].status = $incycle;
					json[idx].fontColor = "white";
					json[idx].backgroundColor = "green";
				};
				
				if(data.isOld=="OLD"){
					json[idx].status = $not_operating;
					json[idx].fontColor = "white";
					json[idx].backgroundColor = "black";
				};
				
				/*csvOutput += (idx+1) + "," +
					data.name + "," + 
					data.stopDate + "," + 
					data.stopTime + "," + 
					status +  "," +
					data.inputDate + "LINE";*/
		
			});
			
			noConnList=json;
			
			for(var i=json.length-1;i>=0;i--){
				console.log(json[i])
				if(json[i].status=='미가동'){
					noConnList.push(json[i]);
					json.splice(i,1);
				}
			}
			
			/*console.log(json)
			console.log(noConnList)*/
			
			$(json).each(function(idx, data){
			
				tr += "<tr style='font-Size:" + getElSize(35) + "px;' class='content '" + className + "' ondblClick=goSingleChart("+'"'+data.name+'"'+","+data.dvcId+")>"  +
				"<td  >" + (idx+1)  + "</td>" +
				"<td  > " + data.name + "</td>" +
				"<td >" + data.stopDate + "</td>" +
				"<td >" + data.stopTime + "</td>" + 
				"<td style='color:" + data.fontColor + "; background-color:" + data.backgroundColor + "; padding : " + getElSize(15) + "px;'>" + data.status + "</td>" +
				"<td >" + data.inputDate + "</td>" +
				"</tr>";
				
				jigData += (idx+1) + "," + 
							data.name + "," +  
							data.stopDate + "," + 
							data.stopTime + "," + 
							data.status + "," + 
							data.inputDate + "LINE";
				
			})
			

			if(json.length==0){
				tr += "<tr  style='border:1px solid white; font-Size:" + getElSize(30) + "px;'>" + 
						"<td colspan='6'>없음</tb>" + 
					"<tr>";
			};
			
			$(".tmpTable").append(tr);
			
			
			$(".row1").not(".tr_table_fix_header").css({
				"background-color" : "#222222"
			});

			$(".row2").not(".tr_table_fix_header").css({
				"background-color": "#323232"
			});
			
			$("#wrapper div:last").remove();
			scrolify($('.tmpTable'), getElSize(1600));
			
			$("#wrapper div:last").css("overflow", "auto")
			//setEl();			
		}
	});
}

function goSingleChart(name, dvcId){
	console.log(name, dvcId)
	if(dvcId!=0){
		window.sessionStorage.setItem("dvcId", dvcId);
		window.sessionStorage.setItem("name", name);
		window.sessionStorage.setItem("date", $("#sDate").val());
		
		window.localStorage.setItem("dvcId",dvcId);
		location.href="/Single_Chart_Status/index.do";
	};
}
